//
//  DZEventCdn.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventCDN {
    public var cdn: String = String()
}

extension DZEventCDN : Encodable {
    enum CodingKeys: String, CodingKey {
        case cdn = "cdn"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cdn, forKey: .cdn)
    }
}
