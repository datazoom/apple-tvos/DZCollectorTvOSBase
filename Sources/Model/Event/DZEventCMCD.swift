//
//  DZEventCMCD.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventCMCD {
    public var sessionId: String = String()
    public var requestId: String = String()
    
    var configuration: [CodingKeys] = []
}

extension DZEventCMCD : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case sessionId = "sid"
        case requestId = "rid"
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.sessionId) {
            try container.encode(sessionId, forKey: .sessionId)
        }
        if configuration.contains(.requestId){
            try container.encode(requestId, forKey: .requestId)
        }
    }
        
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
