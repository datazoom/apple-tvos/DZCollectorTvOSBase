//
//  AdEventType.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 30.1.22..
//  Copyright © 2022 Adhoc Technologies. All rights reserved.
//

import Foundation

public enum AdEventType {
    case none
    
    case adError

    case adBreakStart
    case adBreakEnd
    
    case adRequest
    case adLoaded
    case adComplete
    case adImpression
    case adSkipped
    
    case adClick
    case adPlay
    case adPlaying
    case adPause
    case adResume

    case adPlaybackReady
    case adPlaybackStart
    case adPlaybackComplete
    
    case adMilestone
//    case adFirstQuartile
//    case adMidpoint
//    case adThirdQuartile
    
    case adRenditionChange
    
    
    
    static let defaultType: Self = .none
    
    public var key: String {
        switch self {
        case .none:
            return "none"
            
        case .adError:
            return "error"
            
        case .adBreakStart:
            return "ad_brake_start"
        case .adBreakEnd:
            return "ad_break_end"
            
        case .adRequest:
            return "media_request"
        case .adLoaded:
            return "media_loaded"
            
        case .adComplete:
            return "playback_complete"
        case .adImpression:
            return "ad_impression"
        case .adSkipped:
            return "ad_skip"
            
        case .adClick:
            return "ad_click"
        case .adPlay:
            return "play"
        case .adPlaying:
            return "playing"
        case .adPause:
            return "pause"
        case .adResume:
            return "resume"
            
        case .adPlaybackReady:
            return "playback_ready"
        case .adPlaybackStart:
            return "playback_start"
        case .adPlaybackComplete:
            return "playback_complete"
            
        case .adMilestone:
            return "milestone"
//        case .adFirstQuartile:
//            return "milestone"
//        case .adMidpoint:
//            return "milestone"
//        case .adThirdQuartile:
//            return "milestone"
            
        case .adRenditionChange:
            return "rendition_change"
        }
    }
}

extension AdEventType : Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.key)
    }
}

extension AdEventType : Identifiable {
    public var id: Self { self }
}

extension AdEventType : Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.hashValue == rhs.hashValue
      }
}

