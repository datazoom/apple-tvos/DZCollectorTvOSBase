//
//  VideoType.swift
//  DZ_Adapter_tvOS_Native
//
//  Created by Vuk on 20.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public enum VideoType {
    case none
    
    case content
    case ad
    
    public var key: String {
        switch self {
        case .none:
            return ""
    
        case .content:
            return "Content"
        case .ad:
            return "Ad"
        }
    }
}

extension VideoType : Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.key)
    }
}

extension VideoType : Identifiable {
    public var id: Self { self }
}

extension VideoType : Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.hashValue == rhs.hashValue
      }
}
