//
//  DZConstants.swift
//  DataZoom
//
//  Created by Momcilo Stankovic on 05/04/21.
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
#if !os(macOS)
import UIKit
#endif

//MARK:- Get Actual Flow
func getActualFlow(inseconds:Float64)->String{
    var durationText:String {
        let hours:Int = Int(inseconds / 3600)
        let minutes:Int = Int(inseconds.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds:Int = Int(inseconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i.%02i", minutes, seconds)
        }
    }
    return durationText
}


class DZConstants: NSObject {
    struct Platformz {
        static let isSimulator: Bool = {
            #if arch(i386) || arch(x86_64)
            return true
            #else
            return false
            #endif
        }()
    }
    
    struct notifications {
        static let wsConnectedMsg = "websocket is connected"
        static let wsDisconnectedMsg = "websocket is disconnected:"
        static let wsGotText = "got some text: "
        static let wsGotData = "got some data:"
        static let badJSON = "Bad JSON"
    }
}
