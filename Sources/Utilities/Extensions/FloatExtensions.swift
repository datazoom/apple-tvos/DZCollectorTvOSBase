//
//  FloatExtensions.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 19.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public extension Float {
    func rounded(rule: NSDecimalNumber.RoundingMode, scale: Int) -> Float {
        var result: Decimal = 0
        var decimalSelf = NSNumber(value: self).decimalValue
        NSDecimalRound(&result, &decimalSelf, scale, rule)
        return (result as NSNumber).floatValue
    }
}
