//
//  StringExtension.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public extension String {
    func base64Encode() -> String {
        let utf8str = self.data(using: String.Encoding.utf8)
        let base64Encoded = utf8str?.base64EncodedString()
        return base64Encoded!
    }
}
