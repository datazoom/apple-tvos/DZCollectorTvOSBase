//
//  DZEventCollector.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk Simovic on 2.3.22..
//  Copyright © 2021 RedCellApps. All rights reserved..
//

import AVFoundation
#if !os(macOS)
import UIKit
#endif

import Reachability

public class DZEventCollector {
    public static let shared = DZEventCollector()
    let reachability = try! Reachability()
    
    public var ppath                        = String()
    public var connectionURL                = String()
    public var eventCount = 0
    
    public var eventError: DZEventError = DZEventError()
    public var device: DZEventDevice = DZEventDevice()
    public var network: DZEventNetworkDetails = DZEventNetworkDetails()
    public var geoLocation: DZEventGeoLocation = DZEventGeoLocation()
    public var page: DZEventPage = DZEventPage()
    public var cdn: DZEventCDN = DZEventCDN()
    public var sampling: DZEventSampling = DZEventSampling()
    
    public var cmcd: DZEventCMCD = DZEventCMCD()
    public var adData: DZEventAd = DZEventAd()
    
    public var userDetails: DZEventUserDetails = DZEventUserDetails()
    public var player: DZEventPlayer = DZEventPlayer()
    public var video: DZEventVideo = DZEventVideo()
    public var details: DZEventDetails = DZEventDetails()
    
    public var playerMetadata: [String:Any]? = nil
    public var sessionMetadata: [String:Any]? = nil
    public var customMetadata: [String:Any]? = nil
    
    public var viewID = String()

    public var oAuthToken = String()

    
    public var toMilliss = Int()
    public var fromMilliss = Int()
    
    public var errorCount = Int()
    public var videosPlayedCount = 0
    public var adsPlayedCount = Int()

    public var internetServiceProvider = String()

    public var playbackDuration             = 0
    public var numberOfAds                  = Int()
    public var adSystem                     = String()
    public var adWrapper                    = [String]()
    public var adPosition                   = String()
    public var adId                         = String()
    public var adsPlayed                    = Int()
    public var adBreakId                    = String()

    public var viewStartTime                = Int()
    public var milestonePercent             = Double()
    public var msPercent                    = Double()
    
    public var engagementStart              = 0.0
    public var contentRequestTime           = 0.0
    public var adRequestTime                = 0.0
    public var adStartTime                  = 0.0
    public var adCompletedTime              = 0.0
    public var adMilestoneTime              = 0.0
    public var contentStartTime             = 0.0
    public var contentEndTime               = 0.0
    public var playbackDurationContent      = 0
    public var playbackDurationAds          = 0
    public var playbackDurationTotal        = 0
    public var engagementDurationAds        = 0
    public var engagementDurationContent    = 0
    public var startupDurationAds           = 0
    public var startupDurationContent       = 0
    public var startupDurationTotal         = 0
    
    public var unsentMessages = [Data]()

    var rTimer : Timer!
    public var retryMessages = [[String: Any]]()

    
    //Flag For Flux Data
    public var fluxDataFlag: Bool = true
    
    //Flag For DataZoom Internal Automation Purpose
    public var flagForAutomationOnly: Bool = false

    public func initWithConfiguration() {
        network.initFromConfiguration(DZWebBroker.shared.networkDetails)
        geoLocation.initFromConfiguration(DZWebBroker.shared.networkDetails)
        
        userDetails.appSessionStart = self.engagementStart
        userDetails.userAgent = device.userAgent
        
        player.playerName = ""
        player.playerVersion = ""
        player.autostart = false
        player.loop = false
        player.controls = true
        player.fullscreen = true
        
        video.playerWidth = device.ScreenWidth
        video.playerHeight = device.ScreenHeight
        
        DZWebBroker.shared.webBrokerDelegate = self
    }
    
    public func resetCounters() {
        self.videosPlayedCount = 0
        self.eventCount = 0
        self.errorCount = 0
//        self.contentErrorCount = 0
//        self.adsErrorCount = 0
        DZEventCollector.shared.unsentMessages = [Data]()
    }
    
    func createEvent() -> DZEvent {
        let event = DZEvent()
        
        event.device = device
        event.network = network
        event.geoLocation = geoLocation
        event.page = page
        event.cdn = cdn
        event.cmcd = cmcd
        event.adData = adData
        event.player = player
        event.userDetails = userDetails
        event.video = video
        event.details = details
        event.sampling = sampling
        
        event.details.timestamp = Int(Date().getTimestamp())
        event.details.metrics.playerState = (details.metrics.playerState == "") ? "NA" : details.metrics.playerState
        
        var metadata: [String: Any] = sessionMetadata ?? [:]
        metadata.merge(playerMetadata ?? [:]) {(_,new) in new}
        metadata.merge(customMetadata ?? [:]) {(_,new) in new}
        event.customData = metadata
        
        return event
    }
    
    
    open func createEvent(eventType: EventType) -> DZEvent {
        let event = createEvent()
        
        event.type = eventType
        
        let mediaType: VideoType = video.mediaType
        event.initWithConfiguration(meta: DZWebBroker.shared.metaDataList, flux: DZWebBroker.shared.fluxDataList, mediaType: mediaType)
        
//        let now = CACurrentMediaTime()*1000
//        if adCompletedTime == 0 && adStartTime > 0 {
//            let adPlaying = now - adStartTime
//            event.details.metrics.playbackDurationAds = self.playbackDurationAds + Int(adPlaying)
//        }
        
//        self.playbackDurationTotal = Int(((CFAbsoluteTimeGetCurrent() - adStartTime) * 1000))
//        self.playbackDurationContent = Int ((CFAbsoluteTimeGetCurrent() - adCompletedTime) * 1000)
        
        event.details.metrics.playbackDurationContent = self.playbackDurationContent
        event.details.metrics.playbackDuration = self.playbackDurationTotal
        
        switch eventType {
        case .seekEnd:
            event.details.attributes.seekStart = fromMilliss
            event.details.attributes.seekEnd = toMilliss
            
        case .renditionChange:
            print(event.details.attributes.absShift)
            
        
        case .custom(_ , let metadata):
            var newMetadata: [String: Any] = event.customData ?? [:]
            newMetadata.merge(metadata ?? [:]) {(_,new) in new}
            event.customData = newMetadata
            
        default:
            event.details.metrics.playheadPosition = details.metrics.playheadPosition
        }
        
//        if eventType == .seekEnd {
//            event.details.attributes.seekStart = fromMilliss
//            event.details.attributes.seekEnd = toMilliss
//        } else {
//            event.details.metrics.playheadPosition = details.metrics.playheadPosition
//            //event.details.metrics.playheadPosition = String (format: "%.3f", playHeadPosition)
//        }
        
        
        event.details.metrics.numberOfVideos = 1
        event.details.metrics.eventCount = self.eventCount
        
        print("%%%% Create \(event.type.key) Event %%%% content: \(event.video.mediaType.key)\n")
        return event
    }
    
    open func createAdEvent(eventType: AdEventType) -> DZEvent {
        let event = createEvent()
        
        switch eventType {
        case .adError:
            event.type = .error
        
        case .adMilestone:
            event.type = .milestone
        
        case .adRenditionChange:
            event.type = .renditionChange
            
        case .adRequest:
            event.type = .mediaRequest
            
        case .adLoaded:
            event.type = .mediaLoaded

        case .adComplete:
            event.type = .mediaLoaded
            
        case .adPlaybackReady:
            event.type = .playbackReady
            
        case .adPlay:
            event.type = .play
        
        case .adPlaying:
            event.type = .playing
            
        case .adPlaybackStart:
            event.type = .playbackStart
        
        case .adPlaybackComplete:
            event.type = .playbackComplete
            
        case .adPause:
            event.type = .pause
            
        case .adResume:
            event.type = .resume
            
            
        case .adSkipped:
            event.type = EventType.adSkipped
                
        case .adBreakStart:
            event.type = EventType.adBrakeStart
            
        case .adBreakEnd:
            event.type = EventType.adBreakEnd
            
        case .adImpression:
            event.type = EventType.adImpression
            
        case .adClick:
            event.type = EventType.adClick
            
        case .none:
            break
        }
        
        let mediaType: VideoType = video.mediaType
        event.initWithConfiguration(meta: DZWebBroker.shared.metaDataList, flux: DZWebBroker.shared.fluxDataList, mediaType: mediaType)
        
        let now = CACurrentMediaTime()*1000
        if adCompletedTime == 0 && adStartTime > 0 {
            let adPlaying = now - adStartTime
            event.details.metrics.playbackDurationAds = self.playbackDurationAds + Int(adPlaying)
        }
        
        return event
    }
    
    
    //MARK:- Trigger Message
    open func triggerMessage(event: DZEvent, rateValue: Float = 0, fromValue: Int = 0, toValue: Int = 0, onCompletion: @escaping (String) -> Void) {
        details.metrics.playheadPosition  = rateValue
        fromMilliss = fromValue
        toMilliss = toValue
        self.eventCount += 1

        do {
            let encoder = JSONEncoder()
            let data: Data = try encoder.encode([event])
            print("%%%% Message data to send %%%% \n \(data)")
            
            if reachability.connection != .unavailable {
                let message = String(decoding: data, as: UTF8.self)
                var eventList = DZWebBroker.shared.eventList
                
                if (DZWebBroker.shared.eventTypesVersion == "v3"){
                    if event.video.mediaType == .ad {
                        eventList = DZWebBroker.shared.eventAdList
                    }
                    else {
                        eventList = DZWebBroker.shared.eventContentList
                    }
                }
                
                if eventList.contains(event.type.key) {
                    print("%%%% Event: \(event.type.key) %%%% \n")
                    print("%%%% Message to send: %%%% \n \(message)")
                    DZWebBroker.shared.send(message, onSuccess: { (sum) in
                        if self.flagForAutomationOnly == true {
                            DZWebBroker.shared.postMessageForAutomation(["jsonPayLoad" :message], onSuccess: { (sum) in
                                onCompletion(sum!)
                            })
                        } else {
                            print("%%%% Automation off %%%% \n")
                        }
                    })
                }
                else if event.type.key.components(separatedBy: "_").first == "Custom" {
                    DZWebBroker.shared.send(message, onSuccess: { (sum) in
                        print("%%%% Message sent (Custom Event): %%%% \n \(message)")
                        if self.flagForAutomationOnly == true && DZWebBroker.shared.runId != ""  {
                            DZWebBroker.shared.postMessageForAutomation(["jsonPayLoad" :message], onSuccess: {(sum) in
                                onCompletion(sum!)
                            })
                        } else {
                          print("%%%% Automation off %%%% \n")
                        }
                    })
                }
                else {
                    print("\(event.type.key) is not in list")
                }
            } else {
                NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
                do {
                    try self.reachability.startNotifier()
                }
                catch let error {
                    print("%%%% Error occured while starting reachability notifications: %%%% \n \(error.localizedDescription)")
                }
                
                unsentMessages.append(data)
            }
        }
        catch let error {
            print("%%%% Error: %%%% \n \(error))")
        }
    }
}


extension DZEventCollector {
    //MARK:- Reachability Changed
    @objc func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        print ("%%%%%%%%%%%% Reachability Event Collector: %%%% \n \(reachability.connection)")
        if reachability.connection != .unavailable{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 6) { // change 2 to desired number of seconds
                // Your code with delay
                if !self.unsentMessages.isEmpty{
                    for singleJSON in self.unsentMessages{
                        if let data = try? JSONSerialization.data(withJSONObject: singleJSON, options: []),
                           let stringInput = String(data: data, encoding: .utf8) {
                            let data = stringInput.data(using: .utf8)!
                            do {
                                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>] {
                                    print("%%%% Array JSON: %%%% \n \(jsonArray)")
                                } else {
                                }
                            } catch let error {
                                print("%%%% Error: %%%% \n \(error)")
                            }
                            
                            DZWebBroker.shared.send(stringInput, onSuccess: {(sum) in
                                //print(sum)
                                print("flag ::: \(self.flagForAutomationOnly)")
                                if self.flagForAutomationOnly == true {
                                    DZWebBroker.shared.postMessageForAutomation(["jsonPayLoad" :stringInput], onSuccess: {(sum) in
                                        print("%%%% Automation sum: %%%% \n \(String(describing: sum))")
                                        
                                    })
                                }
                            })
                            
                        }
                    }
                }
            }
            print("%%%% Reachability: %%%% vReachable via WiFi")
        } else {
            print("%%%% Reachability: %%%% Network not reachable")
        }
    }
}

extension DZEventCollector : WebBrokerProtocol {
    func responseRetrunToRetry(retry_msg: String) {
        if retry_msg != "successfull_send" {
            let data: Data? = retry_msg.data(using: .utf8)
            let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String:AnyObject]
            print(json ?? "Empty Data")
            self.retryMessages.append(json!)
            rTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(retryMessage), userInfo: nil, repeats: true)
        }
    }
    
    func connectionInterrupted() {
        print("%%%% ReachabilityChanged: %%%% \n  connectionInterrupted")
    }
    
    func connectionSuccessful() {
        print("%%%% ReachabilityChanged: %%%% \n connectionSuccessful")
    }
    
    @objc func retryMessage() {
        if !self.retryMessages.isEmpty{
            for (idx,singleJSON) in self.retryMessages.enumerated() {
                if let data = try? JSONSerialization.data(withJSONObject: singleJSON, options: []),
                   let stringOutput = String(data: data, encoding: .utf8) {

                    DZWebBroker.shared.send(stringOutput, onSuccess: {(sum) in
                        if ((idx < self.retryMessages.count ?  self.retryMessages[idx] : nil ) != nil) {
                            self.retryMessages.remove(at: idx)
                        } else {
                            if self.retryMessages.count > 0 {
                                self.retryMessages.removeFirst()
                            }
                        }
                    })
                }
            }
            if self.retryMessages.isEmpty {
                self.rTimer.invalidate()
            }
        }
    }
}


